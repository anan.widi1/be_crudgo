package controllers

import (
	"be/models"
	"github.com/labstack/echo/v4"
	"net/http"
	"strconv"
)

func DataBarang(c echo.Context) error {
	result, err := models.DataBarang()

	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
	}
	return c.JSON(http.StatusOK, result)
}

func SimpanController(c echo.Context) error {
	kodebarang := c.FormValue("kodebarang")
	namabarang := c.FormValue("namabarang")

	result, err := models.SimpanData(kodebarang, namabarang)

	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
	}

	return c.JSON(http.StatusOK, result)

}

func UpdateController(c echo.Context) error {
	id := c.FormValue("id")
	kodebarang := c.FormValue("kodebarang")
	namabarang := c.FormValue("namabarang")

	conv_id, err := strconv.Atoi(id)

	if err != nil {
		return c.JSON(http.StatusInternalServerError, err.Error())
	}

	result, err := models.UpdateData(conv_id, kodebarang, namabarang)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
	}
	return c.JSON(http.StatusOK, result)

}

func DeleteController(c echo.Context) error {
	id := c.Param("id")

	conv_id, err := strconv.Atoi(id)

	if err != nil {
		return c.JSON(http.StatusInternalServerError, err.Error())
	}

	result, err := models.DeletData(conv_id)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]string{"message": err.Error()})
	}
	return c.JSON(http.StatusOK, result)

}
