package routes

import (
	"be/controllers"
	"github.com/labstack/echo/v4"
	"net/http"
)

func Init() *echo.Echo {
	e := echo.New()
	e.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, "selamat datang di echo")
	})
	e.GET("/barang", controllers.DataBarang)
	e.POST("/create", controllers.SimpanController)
	e.PUT("/update", controllers.UpdateController)
	e.DELETE("/delete/:id", controllers.DeleteController)
	return e
}
